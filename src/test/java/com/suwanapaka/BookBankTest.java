package com.suwanapaka;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class BookBankTest {
    @Test
    public void shouldDepositSuccess() {
        BookBank book = new BookBank("Name", 0);
        boolean result = book.deposit(100);
        assertEquals(true, result);
        assertEquals(100.0, book.getBalance(), 0.00001);
    }

    @Test
    public void shouldDepositNagtive() {
        BookBank book = new BookBank("Name", 0);
        boolean result = book.deposit(-100);
        assertEquals(false, result);
        assertEquals(0, book.getBalance(), 0.00001);
    }

    @Test
    public void shouldWithDrawSuccess() {
        BookBank book = new BookBank("Name", 0);
        book.deposit(100);
        boolean result = book.withdraw(50);
        assertEquals(true, result);
        assertEquals(50, book.getBalance(), 0.00001);
    }

    @Test
    public void shouldWithDrawNagative() {
        BookBank book = new BookBank("Name", 0);
        book.deposit(100);
        boolean result = book.withdraw(-50);
        assertEquals(false, result);
        assertEquals(100, book.getBalance(), 0.00001);
    }

    @Test
    public void shouldWithDrawOver() {
        BookBank book = new BookBank("Name", 0);
        book.deposit(50);
        boolean result = book.withdraw(100);
        assertEquals(false, result);
        assertEquals(50, book.getBalance(), 0.00001);
    }

    @Test
    public void shouldWithDraw100() {
        BookBank book = new BookBank("Name", 0);
        book.deposit(100);
        boolean result = book.withdraw(100);
        assertEquals(true, result);
        assertEquals(00, book.getBalance(), 0.00001);
    }
}
