package com.suwanapaka;

public class BookBank {
    String name;
    double balance;
    BookBank(String name, double balance) {
        this.name = name;
        this.balance = balance; 
    }

    boolean deposit(double money) {
        if (money <= 0) {
            return false;
        }
        this.balance = this.balance + money;
        return true;
    }

    boolean withdraw(double money) {
        if (money <= 0) {
            return false;
        }
        if(money>balance) {
            return false;
        }
        balance = balance - money;
        return true;
    }

    void print() {
        System.out.println(name + " " + balance);
    }

    public String getName() {
        return name;
    }

    public double getBalance() {
        return balance;
    }

}
