package com.suwanapaka;

public class BookBankApp {
    public static void main(String[] args) {
        BookBank suwanapaka = new BookBank("Suwanapaka", 100.0);
        suwanapaka.name = "Suwanapaka";
        suwanapaka.balance = 100.0;
        suwanapaka.print();
        suwanapaka.deposit(50);
        suwanapaka.print();
        suwanapaka.withdraw(50);
        suwanapaka.print();

        BookBank prayood = new BookBank("Prayood",1 );
        prayood.deposit(1000000);
        prayood.withdraw(10000000);
        prayood.print();

        BookBank praweet = new BookBank("Praweet", 10);
        praweet.deposit(10000000);
        praweet.withdraw(1000000);
        praweet.print();


    }

}
